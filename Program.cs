﻿using System;

namespace gp_test
{
    class Program
    {
        static void Main(string[] args)
        {
           //Start the program with Clear();
           Console.Clear();
           
//Create a List<T> that holds peopleʼs name and the month they are born in.
// Add 10 persons to it
// Print the names sorted by the months they are born in. The
// months are in order they appear in the year.

List<Names> persons = new List<Person>();
{
    persons.add(new Person("Felicia", "October"));
    persons.add(new Person("Natalia", "July"));
    persons.add(new Person("Tony", "Feburary"));
    persons.add(new Person("Clinton", "March"));
    persons.add(new Person("Logan", "September"));
    persons.add(new Person("Xavier", "December"));
    persons.add(new Person("Lucky", "May"));
}
           //End the program with blank line and instructions
           Console.ResetColor();
           Console.WriteLine();
           Console.WriteLine("Press <Enter> to quit the program");
           Console.ReadKey();
        }
    }
}
